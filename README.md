# `F1 Angular`

## Getting Started

To get you started you can simply clone the `f1angular` repository and install the dependencies:

### Prerequisites

You need git to clone the repository. You can get git from [here][git].

You must have Node.js and its package manager (npm) installed. You can get them from [here][node].

### Clone `f1angular`

Clone the `f1angular` repository using git:

```
git clone https://amineatallah@bitbucket.org/amineatallah/f1angular.git
cd f1angular
```

### Install Dependencies

```
npm install
```

### Run the Application

the project has a simple development web server.

```
npm start
```

Now browse to the app at `localhost:3000`.


## Directory Layout

```
app/                             --> all of the source files for the application.
  app.css                        --> default stylesheet.
  components/                    --> all app specific modules.
    common/                      --> has common comopnents for the app.
      loader.component.js        --> the load component.
    home/                        --> home page files.
      home.controller.js         --> home controller fetch the specified seasons.
      home.controller.spec.js    --> home controller tests.
      home.html                  --> home template.
      winnerCard.component.js    --> custom component for the winner ui.
      winnerCard.html            --> winnerCard template.
    details/                     --> details page files.
      details.controller.js      --> details page controller to fetch a specific season.
      detailsPage.html           --> details page template.
      season.comopnent.js        --> custom component for the season ui.
      season.html                --> season template.
  services/                      --> container the app common services.
    helpers.service.js           --> helper functions.
    localStorage.service.js      --> service to cache the data localy, used to cache the winners to use in the details section.
  public/                        --> app public files.
    images/                      --> app images.
    styles/                      --> app styles.
  modules/                       --> contains other modules.
    data/                        --> data fetching module files.
      data.module.js             --> the module provider and service in one file since it's small.
      data.module.spec.js        --> data module DataFetcher tests.
  app.module.js                  --> application main module.
  app.config.js                  --> application main config.
  app.routes.js                  --> application routes.
  app.mainController.js          --> application main controller.
index.html                       --> app layout file (the main html template file of the app)
karma.conf.js                    --> config file for running unit tests with Karma
```

## Testing

### Running Unit Tests

The app comes preconfigured with unit tests. These are written in [Jasmine][jasmine],
which we run with the [Karma][karma] test runner. We provide a Karma configuration file to run them.

* The configuration is found at `karma.conf.js`.
* The unit tests are found next to the code they are testing and have an `.sepc.js` suffix.

** Some parts of the application are tested not all **

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```
This script will start the Karma test runner to execute the unit tests. Moreover, Karma will start
watching the source and test files for changes and then re-run the tests whenever any of them
changes.

## Framework, libraries and tools used.

* [Angularjs](https://angularjs.org/)
* [git](https://git-scm.com/)
* [bower](http://bower.io/)
* [npm](https://www.npmjs.org/)
* [karma](https://karma-runner.github.io/)
* [jasmine](https://jasmine.github.io/)

[angularjs]: https://angularjs.org/
[bower]: http://bower.io/
[git]: https://git-scm.com/
[http-server]: https://github.com/indexzero/http-server
[jasmine]: https://jasmine.github.io/
[karma]: https://karma-runner.github.io/
[node]: https://nodejs.org/
[npm]: https://www.npmjs.org/
