var gulp = require('gulp');
var inject = require('gulp-inject');


gulp.task('inject-js', function () {
  var target = gulp.src('./index.html');
  var sources = gulp.src(['./app/*.module.js', './app/**/*.js', '!./app/bower_components/**/*.js'], {read: false});

  return target.pipe(inject(sources))
    .pipe(gulp.dest('./'));
});
