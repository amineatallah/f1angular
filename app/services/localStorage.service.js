(function(){
  'use strict';

  angular.module('f1').service('LocalStorage', LocalStorage);

  LocalStorage.$inject = ['$q'];

  function LocalStorage($q) {
    this.$q = $q;
  }

  LocalStorage.prototype.saveData = function(name, data) {
    var obj = {};
    data.forEach(function(winner) {
      obj[winner.season] = winner.driverId;
    }, data);
    localStorage.setItem(name, JSON.stringify(obj));
  }

  LocalStorage.prototype.getData = function(name) {
    return this.$q(function(resolve, reject) {
      if (localStorage.getItem(name)) {
          resolve(JSON.parse(localStorage.getItem(name)));
        } else {
          reject('no data of the type ' + name + ' in the localstorage');
        }
    });
  }

})();
