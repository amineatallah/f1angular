(function(){
  'use strict';

  angular.module('f1').factory('helpers', helpers);

  function helpers() {

    var service = {
      changeHomeResponse: changeHomeResponse,
      changeDetailsResponse: changeDetailsResponse
    }
    return service;

    function changeHomeResponse(data) {
      return data.StandingsTable.StandingsLists.map(function(winner, index) {
        return {
          id: (Date.now() + index) * winner.DriverStandings[0].points, // create a unique id.
          season: winner.season,
          driverId: winner.DriverStandings[0].Driver.driverId,
          name: winner.DriverStandings[0].Driver.givenName + ' ' + winner.DriverStandings[0].Driver.familyName,
          nationality: winner.DriverStandings[0].Driver.nationality,
          points: winner.DriverStandings[0].points,
          wins: winner.DriverStandings[0].wins,
          car: winner.DriverStandings[0].Constructors[0].name
        };
      });
    }

    function changeDetailsResponse(data) {
      return data.RaceTable.Races.map(function(race, index) {
        return {
          id: (Date.now() + index) * race.Results[0].laps, // create a unique id.
          raceName: race.raceName,
          date: new Date(race.date).toDateString(),
          winnerId: race.Results[0].Driver.driverId,
          winner: race.Results[0].Driver.givenName + ' ' + race.Results[0].Driver.familyName,
          car: race.Results[0].Constructor.name,
          laps: race.Results[0].laps
        };
      });
    }

  }
})();
