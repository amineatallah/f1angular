(function(){
  'use strict';

  angular.module('f1').config(routesConfig);

  routesConfig.$inject =  ['$routeProvider', '$locationProvider'];

  function routesConfig($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
      templateUrl: './app/components/home/home.html',
      controller: 'HomeController',
      controllerAs: 'homeCtrl'
    })
    .when('/seasons/:year', {
      templateUrl: './app/components/details/detailsPage.html',
      controller: 'DetailsController',
      controllerAs: 'detailsCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
  }
})();
