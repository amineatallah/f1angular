(function(){
  'use strict';

  angular.module('f1').config(MainConfig);

  MainConfig.$inject = ['DataConfigProvider'];

  function MainConfig(DataConfigProvider) {
    DataConfigProvider.setUrl('http://ergast.com/api/f1/');
  }
})();
