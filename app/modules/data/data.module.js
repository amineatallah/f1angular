(function(){
  'use strict';

  angular.module('data', []);
  angular.module('data').provider('DataConfig', DataConfig);
  angular.module('data').service('DataFetcher', DataFetcher);

  function DataConfig() {
    var config = {};
    return {
      setUrl: function(url) {
        config.url = url;
      },
      $get: function() {
        return config
      }
    };
  }

  DataFetcher.$inject = ['$http', '$q', 'DataConfig'];

  function DataFetcher($http, $q, DataConfig) {
    this.$http = $http;
    this.$q = $q;
    this.apiUrl = DataConfig.url;
  }

  DataFetcher.prototype.getData = function(uri) {
    var _this = this;
    return this.$q(function(resolve, reject) {
        _this.$http({
            method: 'GET',
            url: _this.apiUrl + '' + uri
        }).then(function(response) {
            resolve(response.data);
        }, function(reason) {
            reject(reason);
        });
    });
  }
})();
