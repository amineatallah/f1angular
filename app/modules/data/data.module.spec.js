describe('Data Service', function() {
  var DataConfigProvider, DataFetcher, $q, $httpBackend;

  var API = 'http://ergast.com/api/f1/';
  var URI = 'driverstandings/1.json?limit=11&offset=55';

  var RESPONSE_SUCCESS = [{
     id: 1,
     season: '2010',
     driverId: 'alonso',
     name: 'fernando alonso',
     nationallity: 'spanish',
     points: '123',
     wins: '6',
     car: 'ferrari'
  },{
     id: 2,
     season: '2012',
     driverId: 'kimi',
     name: 'kimi raikonnen',
     nationallity: 'Finnish',
     points: '113',
     wins: '9',
     car: 'mercedes'
  }];

  // Load the Data module, initialize the provider and set the api url.
  beforeEach(function() {
    angular.mock.module('data', function(_DataConfigProvider_) {
      DataConfigProvider = _DataConfigProvider_;
      DataConfigProvider.setUrl(API);
    });
  });

  // Inject the DataFetcher service.
  beforeEach(inject(function(_$q_, _$httpBackend_, _DataFetcher_) {
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    DataFetcher = _DataFetcher_;
  }));

  // Verify our service exist.
  it('should exist', function() {
    expect(DataFetcher).toBeDefined();
  });

  describe('getData()', function() {
    var result;

    beforeEach(function() {
      // Initialize our local result object to an empty object before each test
      result = [];

      // Spy on our service call but allow it to continue to its implementation
      spyOn(DataFetcher, "getData").and.callThrough();
    });

    it('should return an array of winners on success', function() {

      // Declare the endpoint we expect our service to hit and provide it with our mocked return values
      $httpBackend.whenGET(API +'' + URI).respond(200, $q.when(RESPONSE_SUCCESS));

      expect(DataFetcher.getData).not.toHaveBeenCalled();
      expect(result).toEqual([]);

      DataFetcher.getData(URI)
                 .then(function(res) {
                    result = res;
                 });

      // Flush pending HTTP requests
      $httpBackend.flush();

      expect(DataFetcher.getData).toHaveBeenCalledWith(URI);
      expect(angular.isArray(result)).toBe(true);
      expect(result.length).toEqual(2);
      expect(result).toEqual(RESPONSE_SUCCESS);

    });
  });
});
