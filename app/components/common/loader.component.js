(function(){
  'use strict';

  angular.module('f1').component('loader', {
    bindings: {
      loadingState: '<',
    },
    template: '<div class="mdl-progress mdl-js-progress" ng-class="{\'mdl-progress__indeterminate\': $ctrl.loadingState}"></div>',
  });
})();
