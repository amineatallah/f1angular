describe('Controller: HomeController', function() {

  beforeEach(angular.mock.module('f1'));
  beforeEach(angular.mock.module('data'));

  var DataFetcher, LocalStorage, helpers, deferred, HomeController, scope;

  var winnersArray = [{
     id: 1,
     season: '2010',
     driverId: 'alonso',
     name: 'fernando alonso',
     nationallity: 'spanish',
     points: '123',
     wins: '6',
     car: 'ferrari'
  },{
     id: 2,
     season: '2012',
     driverId: 'kimi',
     name: 'kimi raikonnen',
     nationallity: 'Finnish',
     points: '113',
     wins: '9',
     car: 'mercedes'
  }];

  beforeEach(inject(function($q, _DataFetcher_, _helpers_, _LocalStorage_) {
    deferred = $q.defer();
    DataFetcher = _DataFetcher_;
    LocalStorage = _LocalStorage_;
    helpers = _helpers_

    spyOn(DataFetcher, 'getData').and.returnValue(deferred.promise);
    spyOn(helpers, 'changeHomeResponse').and.returnValue(winnersArray);
    spyOn(LocalStorage, 'saveData');
  }));

  // Initialize the controller and a mock scope.
  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    HomeController = $controller('HomeController', {
      $scope: scope,
      DataFetcher: DataFetcher
    });
  }));

  describe('DataFetcher.getData', function() {
    it('should call getData', function() {
      expect(DataFetcher.getData).toBeDefined();
      expect(DataFetcher.getData).toHaveBeenCalled();
      expect(DataFetcher.getData.calls.count()).toBe(1);
    });

    it('should return an array of object on success', function() {
      deferred.resolve(winnersArray);
      scope.$apply();

      expect(HomeController.winners).toEqual(winnersArray);
      expect(LocalStorage.saveData).toHaveBeenCalled();
      expect(LocalStorage.saveData.calls.count()).toBe(1);
    });
  });

});
