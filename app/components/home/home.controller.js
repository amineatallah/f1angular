(function(){
  'use strict';

  angular.module('f1').controller('HomeController', HomeController);

  HomeController.$inject = ['$scope','DataFetcher', 'LocalStorage', 'helpers'];

  function HomeController($scope, DataFetcher, LocalStorage, helpers) {
    var vm = this;

    $scope.$emit('loading', { loadingState: true });

    DataFetcher.getData('driverstandings/1.json?limit=11&offset=55')
               .then(function(response) {
                  vm.winners = helpers.changeHomeResponse(response.MRData);
                  LocalStorage.saveData('winners', vm.winners);
                  $scope.$emit('loading', { loadingState: false });
               }, function(reason) {
                  console.log('error', reason);
                  $scope.$emit('loading', { loadingState: false });
               });
  }

})();
