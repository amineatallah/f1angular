(function(){
  'use strict';

  angular.module('f1').component('winnerCard', {
    bindings: {
      winners: '<'
    },
    templateUrl: '/app/components/home/winnerCard.html',
  });
})();
