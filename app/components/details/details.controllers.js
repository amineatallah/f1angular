(function(){
  'use strict';

  angular.module('f1').controller('DetailsController', DetailsController);

  DetailsController.$inject = ['$scope', '$routeParams', 'DataFetcher', 'LocalStorage', 'helpers'];

  function DetailsController($scope, $routeParams, DataFetcher, LocalStorage, helpers) {
    var vm = this;
    vm.year = $routeParams.year;

    $scope.$emit('loading', { loadingState: true });

    DataFetcher.getData(vm.year + '/results/1.json')
               .then(function(response) {
                 vm.races = helpers.changeDetailsResponse(response.MRData);
                 $scope.$emit('loading', { loadingState: false });
               }, function(reason) {
                  console.log('error', reason);
                  $scope.$emit('loading', { loadingState: false });
               });

    // check if winner is in local storage if not make a request to the api to get the winner
    LocalStorage.getData('winners')
                .then(function(response) {
                  vm.winnerId = response[vm.year];
                },function(reason) {
                  DataFetcher.getData(vm.year + '/driverStandings/1.json')
                             .then(function(response){
                                vm.winnerId = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver.driverId;
                             });
                });
  }

})();
