(function(){
  'use strict';

  angular.module('f1').component('season', {
    bindings: {
      races: '<',
      winnerId: '@'
    },
    templateUrl: '/app/components/details/season.html',
  });
})();
