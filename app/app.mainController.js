(function(){
  'use strict';

  angular.module('f1').controller('MainController', MainController);

  MainController.$inject = ['$scope'];

  function MainController($scope) {

    this.loadingState = false;

    $scope.$on('loading', function(event, args) {
      this.loadingState = args.loadingState;
    }.bind(this));
  }

})();
